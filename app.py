from models.customers import database
from flask import Flask, jsonify
import json

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

@app.route("/")
def main():
    return "Welcome!"

@app.route("/users")
def showUsers():
    dbresult = mysqldb.showUsers()
    result = []
    for items in dbresult:
        user = {
            "id" : items[0],
            "username" : items[1],
            "firstname" : items[2],
            "lastname" : items[3],
            "email" : items[4],
        }
        result.append(user)
    return jsonify(result)

if __name__ == "__main__":
    mysqldb = database()
    if mysqldb.db.is_connected():
        print('Connected to MySQL database')
    
    app.run(debug = True)    
    #isi dengan fungsi yang kalian butuhkan
    #jangan lupa untuk menyesuaikan isi variabel data dengan struktur data yang dibutuhkan
    #tambahData(data)

    if mysqldb.db is not None and mysqldb.db.is_connected():
        mysqldb.db.close()




# data = {
#     "params":[
#         {
#             "values" :{
#                 "username" : "userpertama",
#                 "namadepan" : "rudi",
#                 "namabelakang" : "roundhouse",
#                 "email" : "rudi.roundhouse@gmail.com"
#             } 
#         },
#         {
#             "values" :{
#                 "username" : "userkedua",
#                 "namadepan" : "shiroe",
#                 "namabelakang" : "ishigami",
#                 "email" : "shiroe.ishigami@gmail.com"
#             } 
#         },
#         {
#             "values" :{
#                 "username" : "userketiga",
#                 "namadepan" : "akatsuki",
#                 "namabelakang" : "horizon",
#                 "email" : "akatsuki.horizon@gmail.com"
#             } 
#         }
#     ]
# }

# def tambahData(data):
#     for param in data['params']:
#         mysqldb.insertUser(**param)
#     mysqldb.dataCommit()
#     print("data berhasil ditambahkan")

# def ubahData(data):    
#     for param in data['params']:
#         mysqldb.updateUserById(**param)
#     mysqldb.dataCommit()
#     print("data berhasil diubah")

# def hapusData(data):
#     for param in data['params']:
#         mysqldb.deleteUserById(**param)
#     mysqldb.dataCommit()
#     print("data berhasil dihapus")

# def showUsers():
#     mysqldb.showUsers()

# def showUsersById(id):
#     mysqldb.showUsersById(id)
