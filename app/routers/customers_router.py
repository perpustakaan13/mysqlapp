from app import app
from app.controllers import customers_controllers
from flask import Blueprint, request

customers_blueprint = Blueprint("customers_router", __name__)

@app.route("/")
def main():
    return "Welcome Back!"

@app.route("/users", methods=["GET"])
def showUsers():
    return customers_controllers.shows()

@app.route("/user", methods=["GET"])
def showUser():
    params = request.json
    return customers_controllers.shows(**params)

@app.route("/user/insert", methods=["POST"])
def insertUser():
    params = request.json
    return customers_controllers.insert(**params)

@app.route("/user/update", methods=["POST"])
def updateUser():
    params = request.json
    return customers_controllers.update(**params)

@app.route("/user/delete", methods=["POST"])
def deletetUser():
    params = request.json
    return customers_controllers.delete(**params)

@app.route("/user/requesttoken", methods=["GET"])
def requestToken():
    params = request.json
    return customers_controllers.token(**params)